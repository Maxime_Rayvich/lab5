/*
 * Vinh Dat Hoang
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class SkiponacciSequenceTest {
    @Test
    public void testingConstructor() {
        try {
            SkiponacciSequence skipS = new SkiponacciSequence(2, 4, 1);
        } catch (IllegalArgumentException e) {
            System.out.println("Catch Contructor Error is " + e);

        }
    }

    @Test
    public void testingGetTerm() {
        SkiponacciSequence skipS = new SkiponacciSequence(2, 4, 1);
        assertEquals(5, skipS.getTerm(2));
    }

    @Test
    public void testingGetTermWithNegative() {
        SkiponacciSequence skipS = new SkiponacciSequence(-2, 4, -1);
        assertEquals(3, skipS.getTerm(2));
    }

    @Test
    public void testingGetTermForNegativeTerm() {
        SkiponacciSequence skipS = new SkiponacciSequence(2, 4, 1);
        try {
            skipS.getTerm(-1);
            fail("Error negative term caught");
        } catch (IllegalArgumentException e) {

        } catch (Exception e) {
            fail("Wrong exception, should be IllegalArgumentException");
        }
    }

    @Test
    public void testingGetTermForZeroTerm() {
        SkiponacciSequence skipS = new SkiponacciSequence(2, 4, 1);
        try {
            skipS.getTerm(0);
            fail("Error negative term caught");
        } catch (IllegalArgumentException e) {

        } catch (Exception e) {
            fail("Wrong exception, should be IllegalArgumentException");
        }
    }

}

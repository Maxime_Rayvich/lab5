public class SkiponacciSequence extends Sequence{
    private int num1;
    private int num2;
    private int num3;
    public SkiponacciSequence(int num1, int num2, int num3){
        this.num1 = num1;
        this.num2 = num2;
        this.num3 = num3;
    }

    @Override
    public int getTerm(int n){
        if (n < 1){
            throw new IllegalArgumentException("input needs to be 1 or greater.");
        }
        int returnTerm = 1;
        int caseIndex = 1;

        if (n <= 3){
            return sendInitValue(n);
        }

        for (int i = 1; i <= n; i++){
            
            switch(caseIndex){
                case 1: this.num1 += this.num2; returnTerm = this.num1; break;
                case 2: this.num2 += this.num3; returnTerm = this.num2; break;
                case 3: this.num3 += this.num1; returnTerm = this.num2; break;
            }
            caseIndex = (caseIndex>3)?1:caseIndex+1;
        }
        return returnTerm;
    }

    public int sendInitValue(int n){
        switch(n){
            case 1: return num1;
            case 2: return num2;
            default: return num3;
        }
    }
    public String toString(){
        return "this is a SkiponacciSequence with: " + this.num1 + " as the first number and: " + this.num2 + " as the second number and: " + this.num3 + " as the third number";
    }
}

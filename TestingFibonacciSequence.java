/**
 * Maxime Rayvich
 *  2021-10-01
 */
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class TestingFibonacciSequence {
    @Test
    public void testInput(){
       FibonacciSequence fs = new FibonacciSequence(2,4);
       assertEquals(fs.getTerm(1),2);
       assertEquals(fs.getTerm(2),4);
       assertEquals(fs.getTerm(3),6);
       assertEquals(fs.getTerm(4),10);
       assertEquals(fs.getTerm(7),42);
    }

    @Test
    public void testNegativeN(){
        FibonacciSequence fs = new FibonacciSequence(2, 4);
        try{
            fs.getTerm(-1);
            fail("It was supposed to throw an exception for negative input");
        }    
        catch(IllegalArgumentException e){

        }
        catch (Exception e){
            fail("It threw a wrong exception");
        }

        try{
            fs.getTerm(0);
            fail("It was supposed to throw an exception for negative input");
        }    
        catch(IllegalArgumentException e){

        }
        catch (Exception e){
            fail("It threw a wrong exception");
        }
    
    
    }
    
    
    
}

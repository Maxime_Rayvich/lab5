/*
 * Vinh Dat Hoang
 */
public class FibonacciSequence extends Sequence {

    int firstNumber;
    int secondNumber;

    public FibonacciSequence(int firstNumber, int secondNumber) {

        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;

    }

    public int getTerm(int n) {
    
        int firstValue = firstNumber;
        int secondValue = secondNumber;
        int temp = 0;

        if(n < 1){
            throw new IllegalArgumentException("invalid nth value");
        }
        
        if(n == 1){
            return firstNumber;
        }
        if(n == 2){
            return secondNumber;
        }

        firstValue = firstValue + secondValue;
        if (n == 3) {

            return firstValue;

        } else {
            for (int i = 4; i <= n; i++) {

                temp = firstValue;
                firstValue = firstValue + secondValue;
                secondValue = temp;

            }

            int resultOfNth = firstValue;
            return resultOfNth;
        }

    }
    public String toString(){
        return "This is a FibonacciSequence. First number is" + firstNumber + " Second number is " + secondNumber; 
    }
}
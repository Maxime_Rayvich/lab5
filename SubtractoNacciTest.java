import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class SubtractoNacciTest {
    @Test
    public void testConst(){
        SubtractornacciSequence a = new SubtractornacciSequence(5, 8);
    }

    @Test
    public void testCase1(){
        SubtractornacciSequence a = new SubtractornacciSequence(5,8);
        assertEquals(-3, a.getTerm(3));
    }

    @Test
    public void testCase2(){
        SubtractornacciSequence a = new SubtractornacciSequence(-2,6);
        assertEquals(14, a.getTerm(4));
    }

    @Test
    public void testCase3(){
        SubtractornacciSequence a = new SubtractornacciSequence(1,-4);
        assertEquals(14, a.getTerm(5));
    }

    @Test
    public void testCase4(){
        SubtractornacciSequence a = new SubtractornacciSequence(2,2);
        try{
            assertEquals(0, a.getTerm(-1));
        }
        catch(IllegalArgumentException e){
            //pass
        }
        catch(Exception e){
            fail("not right exception");
        }
        
    }
}

/**
 * Maxime Rayvich
 *  2021-10-01
 */

public class SubtractornacciSequence extends Sequence{
    private int numberOne;
    private int numberTwo;
    public SubtractornacciSequence(int num1, int num2){
    
        this.numberOne = num1;
        this.numberTwo = num2;

    }

    public int getTerm(int n){
        if(n <= 0){
            throw new IllegalArgumentException("has to be above zero");
        }
          int num1 = this.numberOne;
            int num2 = this.numberTwo;
            int temp;
            int i = 1;
           
            while( i <= n){
              if(i == n){
                    return num1;
                }
                i++;
            temp = num1;
            num1 = num2;
            num2=temp-num1;

        }
        //should never reach this point
        return -1;
    }
    public String toString(){
        return"this is a SubtractornacciSequence with the first number being: " + this.numberOne + " and the second number being: " + this.numberTwo;
    }

       

}
    


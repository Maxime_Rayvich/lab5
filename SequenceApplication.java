
import java.util.Scanner;
public class SequenceApplication {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String val = in.next();
        int numTries = 2;

        if(validate(val)){
            Sequence[] seq = parse(val);
            for(int i = 1; i <= numTries; i++){
            print(seq, i);
            }
        }
        else{
            System.out.println("Haha you failed wrong input bye");
        }
    }

        
      

    

    static public void print(Sequence[] seq, int n) {

        for (int i = 1; i <= n; i++) {
            for( int j = 0; j < seq.length; j++){
            System.out.println(seq[j].getTerm(i));
            }
        }
    }

    public static Sequence[] parse(String s) {
        String[] arr = s.split(";");
        Sequence[] seqArr = new Sequence[arr.length / 4];
        //should all work ^^

        String tempStr = "";
        int pos1 = 0;
        int pos2 = 0;
        String pos3 = "";


        for (int i = 0; i < seqArr.length; i++) {
           int j= 0;
            while (j < ((i+1)*4)) {
               j = (i*4);
                tempStr = arr[j];
                j++;
                pos1 = Integer.parseInt(arr[j]);
                j++;
                pos2 = Integer.parseInt(arr[j]);
                j++;
                pos3 = arr[j];
                j++;

            }

            if (tempStr.equals("Fib")) {
                seqArr[i] = new FibonacciSequence(pos1, pos2);
            }
            if (tempStr.equals("Sub")) {
                seqArr[i] = new SubtractornacciSequence(pos1, pos2);
            }
            if (tempStr.equals("Skip")) {
                seqArr[i] = new SkiponacciSequence(pos1, pos2, Integer.parseInt(pos3));
            }

        }
        return seqArr;
    }


    public static boolean validate(String s) {
        boolean validateEven4 = false;
        boolean indexZero = false;
        boolean validate4Elem = false;
        boolean validateNext3Index = false;
        boolean finalValidate = false;
        String arrayStr[] = s.split(";");

        // check array for even number of 4
        if (arrayStr.length % 4 == 0) {
            validateEven4 = true;
        }

        // check index at zero to be Fib, Sub or Skip
        if (arrayStr[0].equals("Fib") || arrayStr[0].equals("Sub") || arrayStr[0].equals("Skip")) {
            indexZero = true;
        }

        // check index multiple of 4 to be Fib, Sub or Skip
        for (int i = 4; i <= arrayStr.length; i = i + 4) {
            // if i is at end of array string break out of loop
            if (i > arrayStr.length - 1) {
                break;
            }
            if (arrayStr[i].equals("Fib") || arrayStr[i].equals("Sub") || arrayStr[i].equals("Skip")) {
                validate4Elem = true;
            } else {
                validate4Elem = false;
                break;
            }
        }

        // check the next 3 index after Fib, Sub or Skip
        validateNext3Index = validateNext3Index(arrayStr);

        // condition to passed the test
        finalValidate = (validateEven4 == true) && (indexZero == true) && (validate4Elem == true)
                && (validateNext3Index == true) ? true : false;
                
        return finalValidate;

    }

    public static boolean validateNext3Index(String arrayStr[]) {
        boolean twoNumberAndX = false;
        boolean threeNumber = false;
        boolean validateNext3Index = false;
        int indexAfterI = 3;
    

       
        // check the next 3 index after Fib, Sub or Skip
        for (int i = 0; i <= arrayStr.length; i++) {
            // if i is at end of array string break out of loop
            if (i >= arrayStr.length - 1 || (i + indexAfterI) >=  arrayStr.length ) {
                break;
            }

            if (arrayStr[i].equals("Fib") || arrayStr[i].equals("Sub")) {
                boolean firstNumber = Character.isDigit(arrayStr[i + 1].charAt(0));
                boolean secondNumber = Character.isDigit(arrayStr[i + 2].charAt(0));
                boolean thirdIndex = arrayStr[i + 3].equals("x");

                if (firstNumber == true && secondNumber == true && thirdIndex == true) {
                    twoNumberAndX = true;
                } else {
                    twoNumberAndX = false;
                    break;
                }
            }
            if (arrayStr[i].equals("Skip")) {
                boolean firstNumber = Character.isDigit(arrayStr[i + 1].charAt(0));
                boolean secondNumber = Character.isDigit(arrayStr[i + 2].charAt(0));
                boolean thirdNumber = Character.isDigit(arrayStr[i + 3].charAt(0));
                if (firstNumber == true && secondNumber == true && thirdNumber == true) {
                    threeNumber = true;
                } else {
                    threeNumber = false;
                    break;
                }
            }
        }
        
        validateNext3Index = (twoNumberAndX == true) && (threeNumber == true) ? true : false;
        return validateNext3Index;
    }
}
